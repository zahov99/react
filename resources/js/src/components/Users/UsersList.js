import React, { Component } from 'react';
import ReactTable from "react-table";
import 'react-table/react-table.css';
import axios from 'axios';
import {Link} from 'react-router-dom';
import {changeTitle} from '../../actions/Users';
import {connect} from 'react-redux';

const mapDispatchToProps = (dispatch) => {
    return {
        changeTitle: (title) => {
            return dispatch(changeTitle(title))
        }
    }
}

class ConnectedUsersList extends Component {

    constructor() {
        super();
        
        this.state = {
            users: [],
            users_columns: [{
                Header: 'Name',
                accessor: 'name' // String-based value accessors!
              }, {
                Header: 'Email',
                accessor: 'email',
              }, {
                  Header: 'Action',
                  accessor: 'action',
                  Cell: row => (
                    <div>
                        <Link to={`/user/edit/${row.original.id}`} className="btn btn-primary">Edit</Link>&nbsp;
                        <button className="btn btn-danger" onClick={this.removeUser.bind(this,row.original.id)}>Delete</button>
                    </div>
                  ),
                  sortable: false,
                  width: 140
              }]
        }
    }

    componentDidMount () {
       this.fetchUsers();
       this.props.changeTitle("User List");
    }

    removeUser(id) {
        axios.delete('/api/user/delete/' + id)
        .then(res => this.fetchUsers());
    }

    fetchUsers() {
        axios.get('/api/user/all')
        .then(res => this.setState({
            users: res.data
        }))
    }

    render() {
        return (
            <div>
                <Link to={`/user/new`} className="btn btn-primary">New User</Link>
                <br/>
                <ReactTable
                    data={this.state.users}
                    columns={this.state.users_columns}
                    defaultPageSize={10}
                    minRows={0}
                />
            </div>
        );
    }
}

const UsersList = connect(null, mapDispatchToProps)(ConnectedUsersList);
export default UsersList;