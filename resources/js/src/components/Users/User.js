import React, { Component } from 'react';
import axios from 'axios';
import {Link} from 'react-router-dom';
import {changeTitle} from '../../actions/Users';
import {connect} from 'react-redux';

const mapDispatchToProps = (dispatch) => {
    return {
        changeTitle: (title) => {
            return dispatch(changeTitle(title))
        }
    }
}

class ConnectedUser extends Component {

    constructor() {
        super();
        
        this.state = {
            user: {
                name: '',
                email: ''
            },
            user_additional: {
                password: '',
                password_confirmation: ''
            },
            errors: {},
        }

        this.handleInputChange = this.handleInputChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.fetchUser = this.fetchUser.bind(this);
    }

    componentDidMount () {
        if(this.props.match.params.id) {
            this.fetchUser();
            this.props.changeTitle("Edit User");
        }
        else
        {
            this.props.changeTitle("Add User");
        }
    }

    fetchUser() {
        axios.get('/api/user/get/' + this.props.match.params.id)
        .then(res => this.setState({
            user: res.data,
            user_additional: {
                password: this.props.match.params.id ? '*****' : '',
                password_confirmation: this.props.match.params.id ? '*****' : ''
            }
        }))
    }

    handleInputChange(event,parrent) {
        const target = event.target;
        const type = target.type;
        const value = target.value;
        const name = target.name;
        
        if(type == "password")
        {
            var user_additional =   {...this.state.user_additional};
            user_additional[name] = value;
            this.setState({
                user_additional
            });
        }
        else
        {
            var user =   {...this.state.user};
            user[name] = value;
            this.setState({
                user
            });
        }
    }
      
    handleSubmit(event) {
        event.preventDefault();

        var user = {...this.state.user, ...this.state.user_additional};

        axios.post(this.getRoute(), user )
        .then(res => {
            this.props.history.push("/");
        })
        .catch(res => {
            this.setState({
                errors: res.response.data.errors
            });
        });
    }

    getRoute() {
        if(this.props.match.params.id) {
            return '/api/user/edit/' + this.props.match.params.id;
        }

        return '/api/user/new';
    }
    render() {
        return (
            <div>
                <form onSubmit={this.handleSubmit}>
                    <div className="form-group">
                        <label>
                            Name
                            <input type="text" name="name" className="form-control" value={this.state.user.name} onChange={this.handleInputChange} />
                        </label>
                        {typeof this.state.errors.name !== 'undefined' &&
                            <span className="text-danger">{this.state.errors.name}</span>
                        }
                    </div>
                    <div className="form-group">
                        <label>
                            Email:
                            <input type="text" name="email" className="form-control" value={this.state.user.email} onChange={this.handleInputChange} />
                        </label>
                        {typeof this.state.errors.email !== 'undefined' &&
                            <span className="text-danger">{this.state.errors.email}</span>
                        }
                    </div>
                    <div className="form-group">
                        <label>
                            Password:
                            <input type="password" name="password" className="form-control" value={this.state.user_additional.password} onChange={this.handleInputChange} />
                        </label>
                        {this.state.errors.password !== 'undefined' &&
                            <span className="text-danger">{this.state.errors.password}</span>
                        }
                        </div>
                    <div className="form-group">
                        <label>
                            Confirm Password:
                            <input type="password" name="password_confirmation" className="form-control" value={this.state.user_additional.password_confirmation} onChange={this.handleInputChange} />
                        </label>
                    </div>

                    <Link to={`/`} className="btn btn-primary">Back</Link>
                    &nbsp;
                    <input type="submit" value="Submit" className="btn btn-primary"/>
                </form>

                <br/>
                
            </div>
            
        );
    }
}

const User = connect(null, mapDispatchToProps)(ConnectedUser);
export default User;

