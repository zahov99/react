import React, { Component } from 'react';
import {connect} from 'react-redux';

const mapStateToProps = (state) => {
    return {
        title: state.title
    }
}

class ConnectedLayoutHeader extends Component {
    render() {
        return (
            <div className="card-header">{this.props.title}</div>
        );
    }
}

const LayoutHeader = connect(mapStateToProps, null)(ConnectedLayoutHeader);
export default LayoutHeader;

