import React, { Component } from 'react';
import UsersList from '../Users/UsersList';
import User from '../Users/User';
import LayoutHeader from './LayoutHeader';
import {Route,Switch} from 'react-router-dom';

export default class Layout extends Component {
    render() {
        return (
            <div className="container">
                <div className="row justify-content-center">
                    <div className="col-md-8">
                        <div className="card">
                            <LayoutHeader></LayoutHeader>

                            <div className="card-body">
                                <Switch>
                                    <Route exact path="/" component={UsersList} />
                                    <Route exact path="/user/new" component={User} />
                                    <Route exact path="/user/edit/:id" component={User} />
                                </Switch>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

