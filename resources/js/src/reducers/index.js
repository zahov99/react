import {
    CHANGE_TITLE
} from '../constants/actions';

const initialState = {
    title: ''
};

export const rootReducer = (state = initialState, action) => {
    switch (action.type) {
        case CHANGE_TITLE:
            return {...state, title: action.payload};
        break;
        default:
            return state;
    }
};