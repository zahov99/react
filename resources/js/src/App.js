import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Layout from './components/Layout/Layout';
import {BrowserRouter,Route,Switch} from 'react-router-dom';
import {Provider} from 'react-redux';
import store from './store';

class App extends Component {
    render() {
        return (
            <Layout></Layout>
        );
    }
}

if (document.getElementById('app')) {
    ReactDOM.render(
                    <Provider store={store}>
                        <BrowserRouter>
                            <App />
                        </BrowserRouter>
                    </Provider>, document.getElementById('app'));
}
