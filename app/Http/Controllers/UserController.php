<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\UserRequest;

class UserController extends Controller
{
    //
    public function all() {
    	return User::get();
    }

    public function show($id) {
    	return User::find($id);
    }

    public function new(UserRequest $request) {
        $user = User::create(array_merge($request->except('password'),
                                        ['password' => bcrypt($request->password)]));
    	return $user;
    }

    public function edit(Request $request,$id) {
        $data = $request->except('password');

        if($request->password != "*****")
        {
            array_merge(data,['password' => bcrypt($request->password)]);
        }
        
    	$user = User::find($id)->update($data);
    	$user = User::find($id);
    	return $user;
    }

    public function delete($id) {
    	$user = User::find($id);
    	$user->delete();
    	return $user;
    }
}
