1. composer install
2. copy .env from .env.example
3. set db in .env
4. php artisan migrate --seed
5. npm install && npm run dev