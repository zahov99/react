<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::prefix('user')->group(function () {
    Route::get('/all', 'UserController@all');
    Route::get('/get/{id}', 'UserController@show');
    Route::post('/edit/{id}', 'UserController@edit');
    Route::post('new', 'UserController@new');
    Route::delete('/delete/{id}', 'UserController@delete');
});


