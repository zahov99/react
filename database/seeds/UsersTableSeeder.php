<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create(['name' => 'name1',
        			'email' => 'email1@test.mk',
        			'password' => bcrypt('pass1')
        			]);

        User::create(['name' => 'name2',
        			'email' => 'email2@test.mk',
        			'password' => bcrypt('pass2')
        			]);

        User::create(['name' => 'name3',
        			'email' => 'email3@test.mk',
        			'password' => bcrypt('pass3')
        			]);

        User::create(['name' => 'name4',
        			'email' => 'email4@test.mk',
        			'password' => bcrypt('pass4')
        			]);

        User::create(['name' => 'name5',
        			'email' => 'email5@test.mk',
        			'password' => bcrypt('pass5')
        			]);

        User::create(['name' => 'name6',
        			'email' => 'email6@test.mk',
        			'password' => bcrypt('pass6')
        			]);

        User::create(['name' => 'name7',
        			'email' => 'email7@test.mk',
        			'password' => bcrypt('pass7')
        			]);

        User::create(['name' => 'name8',
        			'email' => 'email8@test.mk',
        			'password' => bcrypt('pass8')
        			]);

        User::create(['name' => 'name9',
        			'email' => 'email9@test.mk',
        			'password' => bcrypt('pass9')
        			]);

        User::create(['name' => 'name10',
        			'email' => 'email10@test.mk',
        			'password' => bcrypt('pass10')
        			]);

        User::create(['name' => 'name11',
        			'email' => 'email11@test.mk',
        			'password' => bcrypt('pass11')
        			]);

        User::create(['name' => 'name12',
        			'email' => 'email12@test.mk',
        			'password' => bcrypt('pass12')
        			]);
        User::create(['name' => 'name13',
        			'email' => 'email13@test.mk',
        			'password' => bcrypt('pass13')
        			]);

        User::create(['name' => 'name14',
        			'email' => 'email14@test.mk',
        			'password' => bcrypt('pass14')
        			]);


    }
}
